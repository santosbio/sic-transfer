<?php

class DataForm
{
    private $dI;
    private $tmp;
    private $d;
    private $h;
    private $s;

    function __construct()
    {

    }

    public function set_data($dI){
        $tmp = explode(' ', $dI);
        $h = $tmp[1];
        $d = explode('-', $tmp[0]);
        $s = substr($d[0], 2, 2);
        $s = $d[2] . '/' . $d[1] . '/' . $s;
        $d = $d[2] . '/' . $d[1] . '/' . $d[0];
        $tmp = $d . ' ' . $h;

        $this->data = $s;
        $this->dataFull = $d;
        $this->hora = $h;
    }

    public function get_data(){
        return $this->data;
    }

    public function get_data_full(){
        return $this->dataFull;
    }

    public function get_hora(){
        return $this->hora;
    }

    
}

function getClientIP() {

    if (isset($_SERVER)) {

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            return $_SERVER["HTTP_X_FORWARDED_FOR"];

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
            return $_SERVER["HTTP_CLIENT_IP"];

        return $_SERVER["REMOTE_ADDR"];
    }

    if (getenv('HTTP_X_FORWARDED_FOR'))
        return getenv('HTTP_X_FORWARDED_FOR');

    if (getenv('HTTP_CLIENT_IP'))
        return getenv('HTTP_CLIENT_IP');

    return getenv('REMOTE_ADDR');
}

 


?>
<!DOCTYPE HTML>
<!--
/*
 * jQuery File Upload Plugin Basic Demo 1.3.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
-->
<html lang="en">
<head>
<!-- Force latest IE rendering engine or ChromeFrame if installed -->
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta charset="utf-8">
<title>Sic Transfer</title>
<meta name="robots" content="noindex">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0, minimal-ui">
<!-- Bootstrap styles -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Generic page styles -->
<link rel="stylesheet" href="css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="css/jquery.fileupload.css">

<style>

    * {
      font-family: 'Roboto', Arial, serif; font-weight: 400;
      font-size: 20px;
    }

    

    html, body {
        background-color: #faa61a;
    }

    .lead {
        color: #FFF;
        font-weight: 600;
        text-align: center;
        line-height: 1;
        padding-top: 20px;
    }

    .fileinput-button {
        position: relative;
        overflow: hidden;
        display: block;
        width: 230px;
        margin: 0px auto -12px;
    }

    .panel-default {
        border-color: rgb(216, 146, 30);
    }

    .panel-default > .panel-heading {
        color: #333;
        background-color: rgb(216, 146, 30);
        border-color: rgb(250, 166, 26);
    }

    .panel-title {
        margin-top: 0px;
        margin-bottom: 0px;
        font-size: 16px;
        color: rgb(255, 255, 255);
    }

    .panel-body {
        padding: 15px;
        color: rgb(75, 75, 75);
    }

    ul {
        list-style: none;
    }

    ul li {
        color: rgb(75, 75, 75);
        font-size: 15px;
    }

    ul li a {
        color: rgb(75, 75, 75);
        font-size: 15px;
    }

    ul li b {
        color: rgb(75, 75, 75);
        font-size: 15px;
    }

    @media screen and (max-width: 900px){
         .time {
           display: none;
        }
    }

    .progress-bar-success {
background-color: #5cb85c;
color: #195E19;
}

#fbuploads {
    display: none;
}

#loginfb {
    display: block;
    width: 100%;
    height: auto;
    text-align: center;
}

#loginfb p {
    display: block;
    font-size: 14px;
    color: #fff;
}


</style>
</head>
<body>

<div class="container">
    <h1><img src="imgs/logo.png" style="display: block; margin: 0 auto;"></h1>
    <h2 class="lead">Para selecionar os arquivos, clique no botão abaixo <br>ou arraste os arquivos para essa janela.</h2>
    
    <br>
    <!-- The fileinput-button span is used to style the file input field as button -->
    <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Selecionar arquivos</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="files[]" multiple>
    </span>
    <br>
    <br>
    <!-- The global progress bar -->
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Links dos arquivos enviados</h3>
        </div>
        <div class="panel-body">
            <ul id="files" class="files">
                
            </ul>
        </div>
    </div>

    

    <div class="panel panel-default" id="fbuploads" style="margin-top: 50px;">
        <div class="panel-heading">
            <h3 class="panel-title" id="meusenvios"></h3>
        </div>
        <div class="panel-body">
            <ul id="files2" class="files">
                
            </ul>
        </div>
    </div>
    
    <div id="loginfb">
    <br>
    <!-- <a href="#" onclick="checkLoginState();">Logar com o Facebook.</a> -->
        <p>Faça login para manter um histórico de envios <br><fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
        </fb:login-button></p>


    </div>
</div>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="js/vendor/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="js/jquery.fileupload.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="h12hcixvuq3w059"></script>


<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//clientes.treynet.com.br/treynet-upload/' : 'file/';

    
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<li/>').html('<a href="http://clientes.treynet.com.br/treynet-upload/file/d/'+file.name+'">http://clientes.treynet.com.br/treynet-upload/file/d/'+file.name+'</a> <a href="mailto:?body=http://clientes.treynet.com.br/treynet-uploadk/file/d/'+file.name+'" target="_blank" style="color: #428bca; font-size: 12px; margin-left: 10px;">Enviar por e-mail</a>').appendTo('#files');
                
                var url = "http://clientes.treynet.com.br/treynet-upload/file/d/"+file.name;
                
                $.ajax({
                  type: "POST",
                  url: "salva-envio.php",
                  data: {fbid: $fbid, fbname: $fbname, url: url}
                });

                
            });

            

            $('#progress .progress-bar').text('Enviado com sucesso.');

            

            setTimeout(function(){
                $('#progress .progress-bar').css(
                    'width',
                    '0%'
                );
                $('#progress .progress-bar').text('');
                
            }, 3000);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').text('Enviando... '+progress+'%');
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>
</body> 
</html>
