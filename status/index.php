<?php
	$xml=simplexml_load_file("https://bitbucket.org/santosbio/sic-transfer/rss?token=404750dc9b910dee24cfc511fdb619c6") or die("Error: Cannot create object");
	// echo '<pre>';		
	// print_r($xml);

	
	
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Sic Transfer - Status</title>
		<meta name="description" content="Blueprint: Vertical Timeline" />
		<meta name="keywords" content="timeline, vertical, layout, style, component, web development, template, responsive" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/default.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<script src="js/modernizr.custom.js"></script>
	</head>
	<body>
		<div class="container">
			<header class="clearfix">
				<span><?php echo $xml->channel->title; ?></span>
				<h1>Últimos updates</h1>
			</header>	
			<div class="main">
				<ul class="cbp_tmtimeline">
				
				<?php
				foreach($xml->channel->item as $Item){
				    //Now you can access the 'row' data using $Item in this case 
				    //two elements, a name and an array of key/value pairs
				    
				    //Loop through the attribute array to access the 'fields'.
				    // foreach($Item->Attribute as $Attribute){
				    //     //Each attribute has two elements, name and value.
				    //     echo $Attribute->Name . ": " . $Attribute->Value;
				    // }
				
						$d = $Item->pubDate;
						$meses = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
						$mesesNum = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
						$d = explode(' ', $d);

						$data = $d[1] . '/' . str_replace($meses, $mesesNum, $d[2]) . '/' . $d[3];
						$h = explode(':', $d[4]);
						$hora = ($h[0]-3) . ':' . $h[1];
						$timestamp = $d[3] . '-' . str_replace($meses, $mesesNum, $d[2]) . '-' . $d[1] . 'T' . $d[4];
						

				?>
					<li>

						<time class="cbp_tmtime" datetime="<?php echo $timestamp; ?>"><span><?php echo $data; ?></span> <span><?php echo $hora; ?></span></time>
						<div class="cbp_tmicon cbp_tmicon-screen"></div>
						<div class="cbp_tmlabel">
							
							<p><?php echo nl2br($Item->title); ?></p>
						</div>
					</li>
				<?php
					}
				?>
				</ul>
			</div>
		</div>

	</body>
</html>
